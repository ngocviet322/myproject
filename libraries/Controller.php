<?php
namespace libraries;

class Controller {

	public function __construct(){
		$this->view=new View();
	}

	public function checkIsset($params,$notNull=false){
		if($notNull==true){
			foreach ($params as $obj => $value) {
				if(!isset($value) == true || empty($value)){
					return false;
				}
			}
		}else{
			foreach ($params as $obj => $value) {
				if(!isset($value)){
					return false;
				}
			}
		}
		return true;
	}

	public function encodeMD5($str){
		return MD5($str.'ngoc viet');
	}

	public function uploadImage($filename){

		session_start();

		$dirname="public/uploads/".$_SESSION['id'];

		if(!file_exists($dirname)){
			mkdir($dirname,0777,true);
		}
		$targetDir=	$dirname.'/';
		$targetFile=$targetDir.basename($_FILES[$filename]["name"]);
		$imageFileType=pathinfo($targetFile,PATHINFO_EXTENSION);
		$uploadOk=1;
		$existFile=0;

		$checkFileSize=getimagesize($_FILES[$filename]["tmp_name"]);

		// Check isImageType
		if($checkFileSize!==false){
			$uploadOk=1;
		}else{
				$uploadOk=0;
		}

		// Check if the file already exist is "upload" Folder .
		$i=0; 
		while(file_exists($targetFile)){
			$targetFile=$targetDir.pathinfo($targetFile,PATHINFO_FILENAME).'_'.$i.".".pathinfo($targetFile,PATHINFO_EXTENSION);
			$i++;
			// $uploadOk=0;
		}
		// Check file Size
		
		if($_FILES[$filename]["size"]>=50000){
			$uploadOk=0;
		}

				// Check limit file type
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
				&& $imageFileType != "gif"){
			$uploadOk=0;
		}
		
		if($uploadOk==0){
			echo $uploadOk;
		}else{
			if(move_uploaded_file($_FILES[$filename]["tmp_name"], $targetFile)){
				echo $uploadOk;
			}else{
				echo $uploadOk;
			}
		}
	}
}
?>