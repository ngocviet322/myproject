<?php
/**
* 
*/
namespace libraries;

class Model
{
	protected $strCon;
	protected $con;
	function __construct()
	{
		$this->strCon="mysql:host=". HOST .";dbname=". DB_NAME .";charset=utf8";
		$this->con=new \PDO($this->strCon,DB_USER,DB_PASS);
		$this->con->setAttribute(\PDO::ATTR_ERRMODE,\PDO::ERRMODE_EXCEPTION);
		$this->con->setAttribute( \PDO::ATTR_EMULATE_PREPARES, false );
	}

	public function getClassName(){
		$className=explode("\\",get_class($this));
		return end($className);
	}

	public function addRecord($params){

		try{
			$sql='INSERT INTO '. $this->getClassName() .'(';
			$subSql='VALUES(';

			$sumEle=count($params);
			$num=1;

			foreach ($params as $key => $value) {			
				if($num===$sumEle){
					$sql.=$key.') ';
					$subSql.=':'.$key.')';
					break;
				}
				$sql.=$key.',';
				$subSql.=':'.$key.',';
				$num++;
			}

			$sql.=$subSql;
			$stmt=$this->con->prepare($sql);

			foreach ($params as $key => &$value) {
				$stmt->bindParam($key,$value);
			}

			$stmt->execute();

			$numRow=$stmt->rowCount();

			if($numRow>0){
				return true;
			}else{
				return false;
			}
			
		}catch(PDOException $e){
			throw $e;
		}

	}

	public function deteleRecord($params){
		$sql='DELETE FROM '.$this->getClassName().' WHERE ';

		foreach ($params as $key => &$value) {
			$sql.=$key.'= :'.$key;
		}
		try{
		$stmt=$this->con->prepare($sql);
		foreach ($params as $key => &$value) {
			$stmt->bindParam(':'.$key,$value);
		}
		$stmt->execute();
		$numRow=$stmt->rowCount();
		if( $numRow>0)
			return $numRow;
		else
			return 0;
		}catch(PDOException $e){
			throw $e;
		}
	}

	public function updateRecord($paramsSet,$paramsCondition){
		$sql='UPDATE '.$this->getClassName().' SET';
		$condition='WHERE';
		$num=1;
		try{
			foreach ($paramsSet as $key => $value) {
				if($num==count($paramsSet)){
					$sql.=' '.$key.'=:'.$key.' ';
					break;
				}
				$sql.=' '.$key.'=:'.$key.',';
				$num++;
			}

			foreach ($paramsCondition as $key => $value) {
				$condition.=' '.$key.'=:'.$key;
			}

			$sql.=$condition;
			$params=array_merge($paramsSet,$paramsCondition);
			
			$stmt=$this->con->prepare($sql);

			foreach ($params as $key => &$value) {
				$stmt->bindParam(':'.$key,$value);				
			}

			$stmt->execute();
			$numRow=$stmt->rowCount();

			if($numRow>0){
				return $numRow;
			}else{
				throw new \Exception("Error Processing Request", 1);		
			}
		}catch(PDOException $e){
			throw $e;
		}
	}


	public function getRecords($params=NULL){
		try{
			$sql="SELECT * FROM ". $this->getClassName();

			if($params!=null){
				$sql="SELECT * FROM ".$this->getClassName()." ORDER BY ". $params[':sortby'] ." DESC 
					LIMIT :start , :display";
				$stmt=$this->con->prepare($sql);

				foreach ($params as $key => &$value) {
					if($key==":sortby") continue;
					$stmt->bindParam($key,$value);
				}	

			}else{
				$stmt=$this->con->prepare($sql);
			}
		
			$stmt->execute();
			$row=$stmt->rowCount();
			
			if($row>0){
				return $stmt->fetchAll(\PDO::FETCH_ASSOC);
			}

		}catch(PDOException $e){throw $e;}
		
		return NULL;
	}

	public function doPaging($params,$page=NULL){
		$display=$params[':display'];
		$start=$params[':start'];

		$sumPage=($page == NULL) ? ceil(count($this->getRecords())/$display) : $page;

		$listRecords=$this->getRecords($params);
 
		return array("list_records"=>$listRecords,"sum_page"=>$sumPage,"begin"=>$start,"limit"=>$display);

	}

	public function __destruct(){

		if($this->con!=NULL){
			$this->con=NULL;
		}
		
	}

}


?>