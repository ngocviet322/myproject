<?php
/**
* 
*/
namespace libraries;
use controllers\error;
use controllers\index;
class Bootstrap
{
	
	public function __construct()
	{

        $url=(isset($_GET['url'])) ? $_GET["url"] : NULL;
		$url=rtrim($_GET['url'],"/");
		$url=explode("/",$url);

        // print_r($url);
        /**
         * Check empty URL
         */
        if(empty($url[0])){
            $controller=new index();
            $controller->init();
            return false;
        }

        $file="controllers/". $url[0] .".php";
        if(file_exists($file)){
            require $file;
        }else{
            $controller=new Error();
            return false;
        }
		
        $controller = ($url[0]!="index") ? new $url[0]() : new index();

        if(isset($url[2])){

            if(method_exists($controller,$url[1])){
                $controller->{$url[1]}($url[2],$url[3]);
            }else{  
                $controller=new Error();
            }
            return false;

        }else{

            if(isset($url[1])){
                if(method_exists($controller,$url[1])){
                    $controller->{$url[1]}();
                }else{  
                    $controller=new Error();
                }
                return false;
            } 

        }
       $controller->init();
	}
}
 
class FileNotFoundException extends \Exception {
    public function __construct(){

        echo "FileNotFoundException";

    }
}
?>