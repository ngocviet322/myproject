<?php
/**
 * 
 */
namespace libraries;

class View{

	public function __construct(){

		// echo "View Controller<br>";
	}

	public function render($name , $noInclude=false){

		if($noInclude==true){			
			include "views/" .$name. ".php";
		}else{
			$name=rtrim($name,'/');
			$name=explode("/",$name);

			if(isset($name[2])){
				include "views/".$name[0]."/header.php";
				include "views/" .$name[0]."/".$name[1]."/".$name[2].".php";
				include "views/".$name[0]."/footer.php";
			}else{
				include "views/header.php";
				include "views/".$name[0]. "/" .$name[1]. ".php";
				include "views/footer.php";
			}

		}

	}
}

?>