<?php

use libraries\Controller;
use models\Manager;

class Login extends Controller{
	private $model;
	public function __construct(){
		
		parent::__construct();

	}

	public function init(){
		$this->view->render("login/index",1);
	}

	public function dologin(){
		$username=$_POST['username'];
		$userpass=$_POST['userpass'];
		$userpass=$this->encodeMD5($userpass);

		if(!$this->checkIsset(array($username,$userpass))){
			header("location:../login");
			exit;
		}
		
		$manager=new Manager();
		if($row=$manager->doLogin($username,$userpass)){
			session_start();
			$_SESSION['id']           =$row['manager_id'];
			$_SESSION['user_name']    =$row['username'];
			$_SESSION['access_level'] =$row['access_level'];
			header("location:../dashboard");
		}else{
			header("location:../login");
		}

	}

	

	
}

?>