<?php
use libraries\Controller;
use models\Manager;
use models\Category;
use models\Article;
class DashBoard extends Controller{
	private $model;
	public function __construct(){

		parent::__construct();
		session_start();
		if(!isset($_SESSION['id'])){
			header('location:login');
		}
		
	}

	public function init(){
		$this->view->render("dashboard/index/index");
	}	

	public function logout(){
		if($_SESSION['id']==true){
			session_destroy();
			header('location:../login');
		}
	}

	public function viewUser($start=NULL,$page=NULL){
		$model=new Manager();
		$display=5;
		if($start==NULL){
			$params=array(':start'=>0,':display'=>$display,':sortby'=>"manager_id");
		}else{
			$start=($start-1)*$display;
			$params=array(':start'=>$start,':display'=>$display,':sortby'=>"manager_id");
		}
		$this->view->js='<script type="text/javascript" src='.URL.'/public/javascript/myajax.js></script>';
		$this->view->manager=$model->doPaging($params,$page);
		$this->view->render("dashboard/user/index");

	}

	public function viewAddUser(){
		$this->view->js='<script type="text/javascript" src='.URL.'/public/javascript/myajax.js></script>';
		$this->view->render("dashboard/user/newuser");
	}

	public function viewCate($start=NULL,$page=NULL){
		$model=new Category();
		$display=5;
		if($start==NULL){
			$params=array(':start'=>0,':display'=>$display,':sortby'=>"cate_id");
		}else{
			$start=($start-1)*$display;
			$params=array(':start'=>$start,':display'=>$display,':sortby'=>"cate_id");
		}
		$this->view->js='<script type="text/javascript" src='.URL.'/public/javascript/myajax.js></script>';
		$this->view->cate=$model->doPaging($params,$page);
		$this->view->render("dashboard/category/index");
	}

	public function viewArticle($start=NULL,$page=NULL){
		$model=new Article();
		$display=5;

		if($start==NULL){
			$params=array(':start'=>0,':display'=>$display,':sortby'=>"article_id");
		}else{
			$start=($start-1)*$display;
			$params=array(':start'=>$start,':display'=>$display,':sortby'=>"article_id");
		}

		$this->view->js='<script type="text/javascript" src='.URL.'/public/javascript/myajax.js></script>';
		$this->view->cate=$model->doPaging($params,$page);
		$this->view->render("dashboard/article/index");
	}

	public function viewAddArticle(){
		$cate=new Category();
		$this->view->js="<script type='text/javascript' src='".URL."ckeditor/ckeditor.js'></script><br>";
		$this->view->js.="<script type='text/javascript' src='".URL."public/javascript/myajax.js'></script>";
		$this->view->cate=$cate->getRecords();
		$this->view->render("dashboard/article/newarticle");
	}
}

?>