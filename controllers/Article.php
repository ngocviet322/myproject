<?php 
use libraries\Controller;
use models\Article as Model_Article;
use controllers\Error;

class Article extends Controller{
	public function __construct(){
		parent::__construct();
		session_start();
		if(!isset($_SESSION['id'])){
			header('location:login');
		}
	}

	public function add(){
		$title=$_POST['title'];
		$cate_id=$_POST['cate_id'];
		$image=$_FILES['image']['name'];
		$intro=$_POST['intro'];
		$author=$_POST['author'];
		$content=$_POST['content'];
		$managerId=$_SESSION['id'];
		$params=array(
			'title'=>$title,
			'cate_id'=>$cate_id,
			'image'=>$image,
			'intro'=>$intro,
			'author'=>$author,
			'content'=>$content,
			'manager_id'=>$managerId
			);

		$art=new Model_Article();

		if(!$this->checkIsset($params,1)){
			throw new \Exception("Error Processing Request", 1);
			exit;
		}

		if($art->addRecord($params)){
			header('location:'.URL.'dashboard/viewArticle');
		}else{
			$error=new Error();
			$error->init();
		}
	}

	public function uploadImage(){

		echo parent::uploadImage('filename');
		
	}
}

 ?>