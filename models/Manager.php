<?php
namespace models;
use libraries\Model;
class Manager extends Model{


	public function __construct(){
		parent::__construct();
	}

	public function getManagerId(){
		return $managerId;
	}
	
	public function setManagerId($managerId){
		$this->managerId=$managerId;
	}

	public function doLogin($username,$userpass){
		try{
		
		$sql  ="SELECT * FROM Manager WHERE username=:username AND userpass=:userpass";
		$stmt =$this->con->prepare($sql);
		
		$stmt->bindParam(":username",$username);
		$stmt->bindParam(":userpass",$userpass);
		
		$stmt->execute();
		
		$row  =$stmt->fetch();

		if($row){
			return $row;
		}else{
			return false;
		}
		}catch(PDOException $e){
			throw $e;

		}
		$this->con=NULL;
	}

	public function getRecord($id){

		try{
			$sql  ="SELECT * FROM Manager WHERE manager_id=:id";
			$stmt =$this->con->prepare($sql);

			$stmt->bindParam(":id",$id);

			$stmt->execute();
			$row=$stmt->rowCount();

			if($row > 0){
				$this->con=NULL;
				return $stmt->fetchAll(\PDO::FETCH_ASSOC);	
			}
			
		}catch(PDOException $e){
			throw $e;
		}

	}
}


?>