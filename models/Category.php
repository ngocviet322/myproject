<?php
namespace models;
use libraries\Model;

class Category extends Model{
	public function __construct(){
		parent::__construct();
	}

	public function addCate(){

		try{
			$sql="INSERT INTO Category(cate_name)
			 VALUES(:cate_name)";

			$stmt=$this->con->prepare($sql);

			foreach ($params as $key => &$value) {
				$stmt->bindParam($key,$value);
			}

			$stmt->execute();

			$rowCount=$stmt->rowCount();

			if($rowCount>0){
				return $rowCount;
			}else{
				die("Fail to insert");
			}
		}catch(PDOException $e){
			throw $e;
		}
	}
	
}



?>
