   <section id="main" class="column">
    <h4 class="alert_info">Welcome to the free MediaLoot admin panel template, this could be an informative message.</h4>
        <div class="clear"></div>
      </div>
    </article><!-- end of stats article -->
    <article class="module width_full">
    <header>
      <h3>Thông Tin</h3>
    </header>
    <div class="module_content">
      <p class="displayName" style="display: none;"></p>
      <article class="stats_graph">
        <form method="POST"  class="info_form" id="info_form1" name="info_form" enctype="multipart/form-data" action="<?=URL.'/article/add'?>">
          <ul>
            <li><label for="title">Title</label> <input
              type="text" id="title" name="title"  required/> <span
              class="form_hint">Proper format "Aa-Zz"</span>
            </li>
            <li><label for="catename">Category</label>
               <select name="cate_id">
          <?php foreach ($this->cate as $key=>$value){ ?>
                  <option value="<?=$this->cate[$key]['cate_id']?>"><?=$this->cate[$key]['cate_name']?></option>                      
          <?php } ?>
              </select>
            </li>
            <li><label for="image">Image :</label> <input
              type="file" id="image" name="image" required/> <span
              class="form_hint">Proper format "Aa-Zz"</span>
              <div id="progressbox">
                <div id="progressbar">
                </div>
                <span class="textStatus">Uploading</span>
              </div>
            </li>

            <li><label for="intro">Tóm Tắt :</label><textarea rows="8" cols="10" name="intro"required></textarea> <span class="form_hint">Proper
                format "Aa-Zz"</span>
            </li>
            <li><label for="author">Author</label> <input
              type="text" id="author" name="author" required/> <span
              class="form_hint">Proper format "Aa-Zz"</span>
            </li>
            <li><label for="content">Nội Dung :</label><br> <br>
              <textarea id="editor1" classname="editor1" name="content" cols="80" rows="10" required></textarea>
            </li> 
            <li>
              <button class="submit" type="submit">Submit</button>
            </li>
          </ul>

          <?php //echo '/'.ltrim(str_replace("/index.php", "", $_SERVER['PHP_SELF']),"/").'/upload/';
              $editor="<script>
              CKEDITOR.replace( 'editor1',
              {
                  filebrowserBrowseUrl :'".URL."ckfinder/ckfinder.html',
                  filebrowserImageBrowseUrl : '".URL."ckfinder/ckfinder.html?type=Images',
                  filebrowserFlashBrowseUrl : '".URL."ckfinder/ckfinder.html?type=Flash',
                  filebrowserUploadUrl : '".URL."ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                  filebrowserImageUploadUrl : '".URL."ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                  filebrowserFlashUploadUrl : '".URL."ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
               });
              </script>";
              echo $editor;
          ?>
        
        </form>
      </article>
      <div class="clear"></div>
    </div>
  </article>
    <div class="clear"></div>  
    <div class="spacer"></div>
  </section>